============================
Cookiecutter Python Template
============================

Cookiecutter_ template for python projects. This template will setup
basic structure to start a python project. It can also be used over an
existing project.


* `Latest built documentation`_

.. _latest built documentation: http://cookiecutter-python.readthedocs.io/en/latest/index.html
.. _cookiecutter: https://github.com/audreyr/cookiecutter
