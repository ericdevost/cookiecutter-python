.. _contributing:

============
Contributing
============

Contributions to the ``cookiecutter-python`` template project is highly
welcome and encouraged ! You can contribute in many ways.

Report issues
~~~~~~~~~~~~~

You can report any issues with the project in the
`source code on GitLab`_

.. _source code on gitlab: https://gitlab.com/ericdevost/cookiecutter-python
