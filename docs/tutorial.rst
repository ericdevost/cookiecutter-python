Tutorial
========

This tutorial will get you started in using this
template to create a python project.

Step 1: Install Cookiecutter
----------------------------

You can use your system python to start your python project,
or use a ``virtualenv``. This tutorial will use the ``virtualenv`` method.

First, you need to create and activate your virtualenv for your python project.
Use your favorite method, or create a virtualenv for your project like
this::

    virtualenv -p python2.7 ~/.virtualenvs/your_python_project

Here, ``your_python_project`` is the name of the python project that
you'll create.

.. note::

   You can use this template on an existing project !

Activate your environment::

    source ~/.virtualenvs/serverdoc/bin/activate

You can got back anytime to the system python by typing::

  deactivate


On Windows, activate it like this. You may find that using a Command Prompt
window works better than gitbash.

.. code-block:: powershell

    > \path\to\env\Scripts\activate


On both platform, install cookiecutter with pip::

    pip install cookiecutter


Step 2: Generate your project
-----------------------------

Now it's time to generate your python project.

Use cookiecutter, pointing it at the cookiecutter-python repo::

    cookiecutter https://gitlab.com/ericdevost/cookiecutter-python.git

.. note::

   If you want to use cookiecutter-python over an existing project,
   you can add the ``-f`` flag to the above command, meaning to
   not fail if the project exist.

You'll be asked to enter values to set your project up.
If you don't know what to enter, stick with the defaults. You can find
a detailed documentation on the values in the :ref:`prompts` section of
this documentation.


Step 3: Build your python project locally
-----------------------------------------

Once your project had been set up with cookiecutter, you
can install the needed dependencies for the documentation.

.. code-block:: console

   $ python -m pip install -r requirements/_docs.txt


You are now ready to build your documentation locally with
one simple command:

.. code-block:: console

  $ python setup.py docs

Your newly built documentation will be accessible in the ``build/docs/html`` folder.

Step 4: Write your doc
----------------------

Make any changes, corrections or additions to your documentation. You can build
it anytime by typing ``python setup.py docs`` and see the changes in your
browser. Once you are satisfied, it's time to publish your documentation

Step 5: Create a GitLab Repo
----------------------------

Go to your GitLab account and create a new repo i. Name it after
``your project name``, where ``your project`` matches the ``project_name``
you entered when you ran cookiecutter.


Step 6: Activate your GitLab repo
---------------------------------

On your computer, enter your newly created ``project folder``, where
``project folder`` is the ``project_name`` you entered when you ran
cookiecutter, then activate your repository::

    cd ``project_name``
    git init .
    git add .
    git commit -m "Initial skeleton."
    git remote add origin your-gitla-repo
    git push -u origin master


Step 8: Build you doc on `Read the docs`_
-----------------------------------------

You can create an account on Read the docs to build your documentation
hosted on GitLab.


.. _read the docs: https://readthedocs.org/
