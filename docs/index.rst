.. cookiecutter-python documentation master file
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to cookiecutter-python documentation!
=============================================

`Cookiecutter`_ template for python projects. This template will setup
basic structure to start a new  ``python project``. It can also be used over an
``existing project`` to generate, for example, your documentation with sphinx_.


* `Source code on GitLab`_

.. _source code on gitlab: https://gitlab.com/ericdevost/cookiecutter-python
.. _sphinx: http://www.sphinx-doc.org/en/stable/

Getting Started
---------------

.. toctree::
   :maxdepth: 2

   tutorial
   troubleshooting
   modules
   contributing

Basics
------

.. toctree::
   :maxdepth: 2

   prompts

Advanced Features
-----------------

.. toctree::
   :maxdepth: 2

   console_script_setup
   baked_project


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. _source code on gitlab: https://gitlab.com/ericdevost/cookiecutter-python
.. _cookiecutter: http://cookiecutter.readthedocs.io/en/latest/
